//
//  ViewController.swift
//  todo
//
//  Created by Deepender Choudhary on 9/7/16.
//  Copyright © 2016 pwc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var item = Item?()
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        let isInAddMode = presentingViewController is UINavigationController
        
        if isInAddMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var nameTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if saveButton === sender {
           let name = nameTextField.text ?? ""
            item = Item(name: name)
        }
    }
    
}

