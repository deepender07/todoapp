//
//  item.swift
//  todo
//
//  Created by Deepender Choudhary on 9/7/16.
//  Copyright © 2016 pwc. All rights reserved.
//

import Foundation


class Item: NSObject,NSCoding {
    
    var name: String
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey("name") as! String
        self.init(name: name)
    }
    
    init?(name: String){
        
        self.name = name
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name")
    }
    

    static let Dir = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    
    static let ArchiveURL = Dir.URLByAppendingPathComponent("items")
    
}